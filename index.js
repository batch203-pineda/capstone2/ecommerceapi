const express = require("express");
const app = express();
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv");


// Routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

dotenv.config();

// This will allow flexibility when using the app locally or as a hosted application.
const port = process.env.PORT || 4000;


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use("/user", userRoutes);
app.use("/product", productRoutes);

// MongoDB connection here!
mongoose.connect(process.env.MongoDB_eCommerceShop)
.then(() =>
    console.log("MongoDB: E-commerce API Connection Successful!"))
.catch((err) => {
    console.log(err);
});




app.listen(port, () => {
    console.log(`Server is running at port ${port}`)
});