const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required: [true, "Product name is required!"]
    },
    description: {
        type: String,
        required: [true, "Description name is required!"]
    },
    price: {
        type: Number,
        required: [true, "Price is required!"]
    },
    availableStock: {
        type: Number
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    orders: [{
        orderId: { type: String },
        userId: { type: String, required: [true, "UserId is required!"] },
        userEmail: { type: String, required: [true, "Email is required!"] },
        quantity: { type: Number },
        purchasedOn: { type: Date, default: new Date() }
    }]
})

module.exports = mongoose.model("Product", productSchema);