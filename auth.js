const jwt = require("jsonwebtoken");

const secret = "ECommerceAPI";



// Token Creation
module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, secret,{});
}


// Token Verification
module.exports.verify = (req, res, next) => {
    
    let token = req.headers.authorization;
    console.log(token);

    if(token !== undefined) {
        token = token.slice(7, token.length);
        console.log(token);
  
        return jwt.verify(token, secret, (err, data)=> {
        // If JWT is not valid
        if (err){
            return res.send({auth: "Invalid token!"});
        }
        // If JWT is valid
        else {
        // Allows the application to proceed with the next middleware function or callback function in the route.
        next();
        }
    })
    }
else {
res.send({message: "Login as Admin to access this page."});
}
}


// Token decryption
module.exports.decode = (token) => {
    if (token !== undefined) {
        token = token.slice(7, token.length);
    
        return jwt.verify(token, secret, (err, data)=> {
            if (err) {
                // if token is not valid
                return null;
            } else {
                // decode method is use to obtain the information form the JWT
                // Syntax: jwt.decode(token, [options]);
                // Returns an object with access to the "payload" property which contains the user information stored when the token is generated.
                return jwt.decode(token, {complete:true})
                .payload;
            }
        })
    } else {
            // if token does not exist
            return null;
    }    
}