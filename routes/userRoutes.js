const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userControllers = require("../controllers/userControllers");

// Check Email Route
router.post("/checkEmail", userControllers.checkEmailExists);

// Registration Route
router.post("/register", userControllers.registerUser);

// Login Route
router.post("/login", userControllers.loginUser);

// User Details Route
router.get("/details/:userId", auth.verify, userControllers.getProfile)

// User Checkout Route
router.post("/checkout", auth.verify, userControllers.getOrders);

// Set User Admin Route
router.put("/:userId/setAsAdmin", auth.verify, userControllers.setUserAdmin);

// Retrieve Auth User's Order Route
//router.get("/myOrders", auth.verify, userControllers.getAuthUsersOrder);



module.exports = router;