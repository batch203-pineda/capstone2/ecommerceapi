const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

// Add Product Route
router.post("/addproduct", auth.verify, productControllers.addProduct);

// View Product (customer)
router.get("/collection", productControllers.viewAllProduct);

// View Product (admin)
router.get("/stock", auth.verify, productControllers.viewAllActiveInActiveProduct);

// View Single Product
router.get("/camera/:productId", productControllers.viewProduct);

// Update Product Route
router.put("/camera/:productId", auth.verify, productControllers.updateProduct);

// Archive Product Route
router.patch("/archive/:productId", auth.verify , productControllers.archiveProduct);


module.exports = router;


 