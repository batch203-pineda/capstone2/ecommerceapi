const User = require("../models/userModel");
const Product = require("../models/productModel");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Check Email
module.exports.checkEmailExists = (req, res) => {
  return User.find({email: req.body.email})
  .then(result => {
    if(result.length > 0){
        return res.send(true);
    }
    else {
      return res.send(false);
    }
  })
  .catch(err => res.send(err));
}


// User registration
module.exports.registerUser = (req, res) => {
    
  let newUser = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password,10),
      mobileNumber: req.body.mobileNumber
  })
  return newUser.save()
  .then(user => {
      console.log(user);
      res.send(true);
  })
  .catch(err => {
      res.send(false)
  })


}



// User Authentication (login)
module.exports.loginUser = (req,res) => {
    return User.findOne({email: req.body.email})
    .then(result => {
        if (result == null){
            return res.send({Message: "No User Found!"});
        }
        else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
            if(isPasswordCorrect){
                return res.send({accessToken: auth.createAccessToken(result)});
            }
            else{
                // password do not match here
                return res.send({message: "Incorrect password"});
            }
        }
    })
}


// Retrieve User Details
module.exports.getProfile = (req, res) => {
    
    const userData = auth.decode(req.headers.authorization);

    return User.findById(userData.id)
    .then(result => {
        result.password = "*******";
        res.send(result);
    })

}

// Set User as Admin
module.exports.setUserAdmin = (req, res) => {
    
    const userData = auth.decode(req.headers.authorization);

    let userId = userData.id;
    
    let updateUserAsAdmin = {
        isAdmin: true
    }

    console.log(updateUserAsAdmin);
    if(userData.isAdmin){
        return User.findByIdAndUpdate(req.params.userId, updateUserAsAdmin ,{new: true})
        .then(result => {
            console.log(result);
            res.send(result);
        })
        .catch(err => {
            console.log(err);
            res.send("Error while setting a User as Admin.")
        })
    }else {
        res.status(401).send("You don't have access to this page!");
    }
}

    // Fix later
//Retrieve Auth User's Order
/* module.exports.getAuthUsersOrder = (req, res ) => {

    const userData = auth.decode(req.headers.authorization);
    
    let userEmail = userData.email;
    // console.log(userEmail);
    return User.find({email: userEmail})
    .then(result => {
        console.log(result);
        res.send(result);
        if(userData.isAdmin == false && result.length > 0) {
            User.orders.find({})
            .then(result => { 
                console.log(result);
                res.send(result);
            })
            .catch(err => { console.log(err);
            });
        }
        else {
            return res.send("Please login to view Order.");
        }
    })
    .catch(err => { console.log(err);
        return res.send("Error while checking authentication.");
    });

} */

// Non-admin User Checkout (Create Order)
    
module.exports.getOrders = async (req, res) => {
        
  const userData = await auth.decode(req.headers.authorization);
  //console.log(userData);

  for (let i = 0; i < req.body.products.length; i++) {
    let productInformation = await Product.findById(req.body.products[i].productId)
      .then(result => {
        console.log(result);
        return result
      })

    let data1 = {
      userId: userData.id,
      email: userData.email,
      price: productInformation.price,
      productId: req.body.products[i].productId,
      quantity: req.body.products[i].quantity,
      productName: productInformation.productName
    }


    if (userData.isAdmin) {
      res.send(`Please use a user account to access this.`)
    } else if (productInformation.stock < req.body.products[i].quantity) {
      res.send(`Order quantity exceeds product on stock`)
    } else {


      // console.log(data)
      // let totalAmount = await (productInformation.price * data.quantity);

      let updateUserOrder = await User.findById(userData.id)
        .then(user => {
          user.orders.push({
            // products: req.body.products,
            totalAmount: data1.price,
            products: {
              productName: data1.productName,
              productId: data1.productId,
              quantity: data1.quantity
              // totalAmount: totalAmount,
              // products: {
              // productName: req.body.productName,

              // }
            }
          });
          return user.save()
            .then(result => {
              console.log(result);
              return true;
            })
            .catch(err => {
              console.log(err);
              return false;
            })
        })


      let data = {
        userId: userData.id,
        email: userData.email,
        // price: productInformation.price,
        productId: req.body.products[i].productId,
        quantity: req.body.products[i].quantity,
        productName: req.body.products[i].productName
      }

      let isProductUpdated = await Product.findById(data.productId)
        .then(product => {
          product.orders.push({
            userId: data.userId,
            userEmail: data.email,
            quantity: data.quantity
          })
          product.availableStock -= data.quantity;
          // res.send(product);
          return product.save()
            .then(result => {
              console.log(result);
              return true;
            })
            .catch(err => {
              console.log(err);
              return false;
            })
        })

      
      if (updateUserOrder && isProductUpdated && i == [req.body.products.length - 1]) {
        // console.log(productPurchased);
        res.send(`Thank you for buying our products!`)

      } else if (i !== [req.body.products.length - 1]) {
        console.log(false)
      } else {
        res.send(false)
      }
      
    }
  }
}




    
          
  
 