const Product = require("../models/productModel");
const auth = require("../auth");


// Create Product (Admin Only)

module.exports.addProduct = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let newProduct = new Product({
        productName: req.body.productName,
        description: req.body.description,
        price: req.body.price,
        availableStock: req.body.availableStock
    });

    if (userData.isAdmin) {
        return newProduct.save()
        .then(product => {
            console.log(product);
            res.send(true);
        })
        .catch(err => {
            console.log(err);
            res.send("Please add product.");
        })
    }else {
        return res.status(401).send(false);
    }
}


// Retrieve All Active Product

module.exports.viewAllProduct = (req, res) => {
    return Product.find({isActive: true})
    .then(result => res.send(result));
}


// Retrieve Single Product

module.exports.viewProduct = (req, res) => {
    return Product.findById(req.params.productId)
    .then(result => res.send(result));

}


// Retrive All Active and InActive Product
module.exports.viewAllActiveInActiveProduct = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        return Product.find({})
        .then(result => res.send(result));
    }
    else {
        return res.status(401).send(false);
    }
}


// Update Product Information
module.exports.updateProduct = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin){
        let updateProduct = {
            productName: req.body.productName,
            description: req.body.description,
            price: req.body.price,
            availableStock: req.body.availableStock,
            isActive: req.body.isActive
        }
        return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new: true})
        .then(result => {
            console.log(result);
            res.send(result);
        })
        .catch(err => {
            console.log(err);
            res.send(false)
        })
    }
    else {
        return res.status(401).send(false)
    }
}


// Archive Product

module.exports.archiveProduct = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let updateIsActiveField = {
        isActive: req.body.isActive
    }

    if(userData.isAdmin){
        return Product.findByIdAndUpdate(req.params.productId, updateIsActiveField)
        .then(result => {
            console.log(result);
            res.send(true);
        })
        .catch(err => {
            console.log(err);
            res.send(false)
        })
        
        
    }else{
        return res.status(401).send(false)
    }

}



